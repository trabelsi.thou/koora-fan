package com.esprit.koorafan.global;

public class Constants {
    public static final long SPLASH_TIME_OUT=3*1000;
    public static final String KEY_NAME = "Name";
    public static final String KEY_EMAIL = "Email";
    public static final String KEY_AGE = "Age";
    public static final String KEY_PASSWORD = "Password";
    public static final String KEY_REMMEMBER = "is_connected";
    public static final String TEAM_TABLE = "teams_table";
    public static final String DB_TEAM = "room_team_db";
}
