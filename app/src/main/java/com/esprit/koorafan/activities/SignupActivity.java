package com.esprit.koorafan.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.TextUtils;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import com.esprit.koorafan.R;
import com.esprit.koorafan.database.AppDataBase;
import com.esprit.koorafan.entity.Team;
import com.esprit.koorafan.global.Constants;
import com.google.android.material.button.MaterialButton;

public class SignupActivity extends AppCompatActivity {
    EditText edtName, edtEmail, edtAge, edtPassword;
    MaterialButton btnNext;
    CheckBox chkRemmember;
    private String name, email, age, password;
    private SharedPreferences preferences;
    private boolean remmember;
    private AppDataBase database;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        initViews();
        retreiveData();
        setupData();
        insertDataInDB();
    }

    private void insertDataInDB() {
        database = AppDataBase.getAppDatabase(this);
        if (database.teamDao().getAll().size() == 0) {
            Team team = new Team("Manshester ", R.drawable.manshester, false);
            database.teamDao().insertOne(team);
            team = new Team("Arsenal ", R.drawable.arseenal, false);
            database.teamDao().insertOne(team);
            team = new Team("Leicester ", R.drawable.leicester, false);
            database.teamDao().insertOne(team);
            team = new Team("Southamton ", R.drawable.south, false);
            database.teamDao().insertOne(team);
            team = new Team("Watford ", R.drawable.watford, false);
            database.teamDao().insertOne(team);
        }
    }

    private void setupData() {
        if (remmember) {
            edtAge.setText(age);
            edtName.setText(name);
            edtEmail.setText(email);
            edtPassword.setText(password);
            chkRemmember.setChecked(remmember);
        }
    }

    private void initViews() {
        btnNext = findViewById(R.id.btn_next);
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signupUser();
            }
        });
        edtAge = findViewById(R.id.edt_age);
        edtEmail = findViewById(R.id.edt_email);
        edtName = findViewById(R.id.edt_name);
        edtPassword = findViewById(R.id.edt_password);
        chkRemmember = findViewById(R.id.chk_remmember);
    }

    private void signupUser() {
        if (isFormFilled()) {
            saveUserInfoToSharedPrefs();
            goToMainActivity();
        }
    }

    private void goToMainActivity() {
        Intent intent = new Intent(SignupActivity.this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private void saveUserInfoToSharedPrefs() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.KEY_NAME, edtName.getText().toString());
        editor.putString(Constants.KEY_EMAIL, edtEmail.getText().toString());
        editor.putString(Constants.KEY_AGE, edtAge.getText().toString());
        editor.putString(Constants.KEY_PASSWORD, edtPassword.getText().toString());
        editor.putBoolean(Constants.KEY_REMMEMBER, chkRemmember.isChecked());
        editor.apply();
    }

    private void retreiveData() {
        preferences = PreferenceManager.getDefaultSharedPreferences(this);
        name = preferences.getString(Constants.KEY_NAME, "");
        email = preferences.getString(Constants.KEY_EMAIL, "");
        age = preferences.getString(Constants.KEY_AGE, "");
        password = preferences.getString(Constants.KEY_PASSWORD, "");
        remmember = preferences.getBoolean(Constants.KEY_REMMEMBER, false);
    }

    private boolean isFormFilled() {
        //error_message_missing_name
        if (TextUtils.isEmpty(edtName.getText().toString())) {
            showErrorMessage(getString(R.string.error_message_missing_name));
            return false;
        }
        if (TextUtils.isEmpty(edtEmail.getText().toString())) {
            showErrorMessage(getString(R.string.error_message_missing_email));
            return false;
        }
        if (TextUtils.isEmpty(edtAge.getText().toString())) {
            showErrorMessage(getString(R.string.error_message_missing_age));
            return false;
        }
        if (TextUtils.isEmpty(edtPassword.getText().toString())) {
            showErrorMessage(getString(R.string.error_message_missing_password));
            return false;
        }
        return true;
    }

    private void showErrorMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}