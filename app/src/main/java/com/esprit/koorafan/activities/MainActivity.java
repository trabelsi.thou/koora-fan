package com.esprit.koorafan.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.TextView;

import com.esprit.koorafan.R;
import com.esprit.koorafan.database.AppDataBase;
import com.esprit.koorafan.entity.Team;
import com.esprit.koorafan.fragments.FavouriteTeamsListFragment;
import com.esprit.koorafan.fragments.ProfileFragment;
import com.esprit.koorafan.fragments.TeamsListFragment;
import com.esprit.koorafan.global.Constants;
import com.google.android.material.button.MaterialButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {


    private AppDataBase database;
    private List<Team> teamsList;
    private TextView tvTeams, tvFavourite, tvProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initUi();
        addFrragment(TeamsListFragment.newInstance("", ""));
    }

    private void initUi() {
        tvTeams = findViewById(R.id.tv_teams);
        tvFavourite = findViewById(R.id.tv_fav);
        tvProfile = findViewById(R.id.tv_profile);
        tvTeams.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFrragment(TeamsListFragment.newInstance("", ""));
            }
        });
        tvFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFrragment(FavouriteTeamsListFragment.newInstance("", ""));
            }
        });
        tvProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFrragment(ProfileFragment.newInstance("", ""));
            }
        });
    }

    private void addFrragment(Fragment newInstance) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.frame_container, newInstance, "")
                .disallowAddToBackStack()
                .commit();
    }


    private void closeApp() {
        finish();
    }
}