package com.esprit.koorafan.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.esprit.koorafan.R;
import com.esprit.koorafan.activities.MainActivity;
import com.esprit.koorafan.adapters.TeamsAdapter;
import com.esprit.koorafan.database.AppDataBase;
import com.esprit.koorafan.entity.Team;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TeamsListFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TeamsListFragment extends Fragment {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private View view;
    private MainActivity activity;
    private AppDataBase database;
    private ArrayList<Team> teamList = new ArrayList<>();
    private RecyclerView rvTeams;

    public TeamsListFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment TeamsListFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static TeamsListFragment newInstance(String param1, String param2) {
        TeamsListFragment fragment = new TeamsListFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_teams_list, container, false);
        activity = (MainActivity) getActivity();
        retreiveTeamsListFromDB();
        initUI();
        return view;
    }

    private void initUI() {
        rvTeams = view.findViewById(R.id.rv_teams);
        rvTeams.setLayoutManager(new LinearLayoutManager(activity));
        TeamsAdapter teamsAdapter = new TeamsAdapter(activity, teamList,false,database);
        rvTeams.setAdapter(teamsAdapter);
    }

    private void retreiveTeamsListFromDB() {
        database = AppDataBase.getAppDatabase(activity);
        teamList.addAll(database.teamDao().getAll());
    }
}