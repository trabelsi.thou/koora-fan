package com.esprit.koorafan.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.esprit.koorafan.R;
import com.esprit.koorafan.database.AppDataBase;
import com.esprit.koorafan.entity.Team;

import java.util.ArrayList;


public class TeamsAdapter extends RecyclerView.Adapter<TeamsAdapter.ViewHolder> {


    private ArrayList<Team> mData = new ArrayList<>();
    private LayoutInflater mInflater;
    boolean isfavourite;
    AppDataBase dataBase;
    Context context;

    // data is passed into the constructor
    public TeamsAdapter(Context context, ArrayList<Team> data, boolean isfavourite, AppDataBase dataBase) {
        this.mInflater = LayoutInflater.from(context);
        this.mData = data;
        this.isfavourite = isfavourite;
        this.dataBase = dataBase;
        this.context = context;
    }

    // inflates the row layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.item_team, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the TextView in each row
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        String tem_name = mData.get(position).getName();
        holder.tvTeamName.setText(tem_name);
        holder.imgTeam.setImageResource(mData.get(position).getImageUrl());
        holder.imgDelete.setVisibility(isfavourite ? View.VISIBLE : View.GONE);
        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mData.get(position).setFavourite(false);
                dataBase.teamDao().update(mData.get(position));
                mData.remove(position);
                notifyItemRangeRemoved(position, 1);
                Toast.makeText(context, "Team removed from favorites", Toast.LENGTH_SHORT).show();
            }
        });
        holder.imgFavourite.setVisibility(isfavourite ? View.GONE : View.VISIBLE);
        holder.imgFavourite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!mData.get(position).isFavourite()) {
                    mData.get(position).setFavourite(true);
                    dataBase.teamDao().update(mData.get(position));
                    Toast.makeText(context, "Team added to favorites", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(context, "Team already exists in favorites", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    // total number of rows
    @Override
    public int getItemCount() {
        return mData.size();
    }

    // stores and recycles views as they are scrolled off screen
    class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvTeamName;
        ImageView imgFavourite;
        ImageView imgDelete;
        ImageView imgTeam;

        ViewHolder(View itemView) {
            super(itemView);
            tvTeamName = itemView.findViewById(R.id.tv_team_name);
            imgDelete = itemView.findViewById(R.id.img_delete);
            imgFavourite = itemView.findViewById(R.id.img_fav);
            imgTeam = itemView.findViewById(R.id.img_team);

        }


    }
}

