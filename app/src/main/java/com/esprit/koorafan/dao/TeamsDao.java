package com.esprit.koorafan.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.esprit.koorafan.entity.Team;
import com.esprit.koorafan.global.Constants;

import java.util.ArrayList;
import java.util.List;

@Dao
public interface TeamsDao {
    @Insert
    void insertOne(Team team);

    @Delete
    void delete(Team team);
    @Update
    void update(Team team);

    @Query("SELECT * FROM " + Constants.TEAM_TABLE)
    List<Team> getAll();
}
