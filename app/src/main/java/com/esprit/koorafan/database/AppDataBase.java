package com.esprit.koorafan.database;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.esprit.koorafan.dao.TeamsDao;
import com.esprit.koorafan.entity.Team;
import com.esprit.koorafan.global.Constants;

@Database(entities = {Team.class}, version = 1, exportSchema = false)
public abstract class AppDataBase extends RoomDatabase {
    private static AppDataBase instance;
    public abstract TeamsDao teamDao();
    public static AppDataBase getAppDatabase(Context context) {
        if (instance == null) {
            instance = Room.databaseBuilder(context.getApplicationContext(),  AppDataBase.class, Constants.DB_TEAM)
                    .allowMainThreadQueries()
                    .build();
        }
        return instance;
    }

}