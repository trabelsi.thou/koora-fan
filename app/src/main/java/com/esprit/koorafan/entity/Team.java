package com.esprit.koorafan.entity;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.esprit.koorafan.global.Constants;

@Entity(tableName = Constants.TEAM_TABLE)
public class Team {
    @ColumnInfo(name = "name")
    String name;

    public int getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(int imageUrl) {
        this.imageUrl = imageUrl;
    }

    @ColumnInfo(name = "image_url")
    int imageUrl;

    public int getUid() {
        return uid;
    }

    public void setUid(int uid) {
        this.uid = uid;
    }

    @PrimaryKey(autoGenerate = true)
    private int uid;

    boolean favourite;

    public Team(String name, int imageUrl, boolean favourite) {
        this.name = name;
        this.imageUrl = imageUrl;
        this.favourite = favourite;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }


}
